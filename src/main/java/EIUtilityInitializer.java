import com.reltio.config.ConfigProperty;
import com.reltio.configuration.utils.PropertiesUtils;
import com.reltio.metadata.uri.TenantId;
import com.reltio.sdk.platform.services.ReltioPlatform;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.SystemConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class EIUtilityInitializer {
    public EIUtilityInitializer(){

    }
    public static final String CONFIG_FILE = "CONFIG_FILE";

    public static Configuration getConfiguration(String configPropertyName) throws Exception {
            String propertyFilePath = System.getProperty(configPropertyName);
            System.out.println("Reading config " + configPropertyName + " from file: " + propertyFilePath);
            if (propertyFilePath == null) return  null;
            File configFile = new File(propertyFilePath);
            InputStream inputStream = new FileInputStream(configFile);
            Configuration config = PropertiesUtils.fromInputStream(inputStream);
            CompositeConfiguration configuration = new CompositeConfiguration();
            configuration.addConfiguration(new SystemConfiguration());
            configuration.addConfiguration(config);
            return configuration;
    }

    public static void main(String[] args) throws Exception {
        System.setProperty("CONFIGS_VALIDATION_ENABLED", Boolean.toString(false));
        Configuration configuration = getConfiguration(CONFIG_FILE);
        if (configuration == null) {
            System.out.println("config can't be read");
        }
        else {
            TenantId tenantId = new TenantId(configuration.getString("tenant"));
            int threads = configuration.getInteger("THREADS", 200);
            int batch = configuration.getInteger("BATCH_SIZE", 2);
            boolean isDryRun = configuration.getBoolean("DRY_RUN", true);
            String startCK = configuration.getString("START_FROM_KEY", null);
            String[] typesArray = configuration.getStringArray("ENTITY_TYPE");
            String inputDataFile = configuration.getString("INPUT_DATA_FILE", null);
            Set<String> types = null;
            if(typesArray != null && typesArray.length > 0){
                types = new HashSet<>();
                for (String type : typesArray) {
                    types.add(type);
                }
            }
            String suffix  = configuration.getString("SUFFIX", "001");
            System.out.println("CONFIGURATION:");
            System.out.println("Cassandra Hosts:              " + configuration.getList(ConfigProperty.CASSANDRA_HOSTS.name()));
            System.out.println("Cassandra Cluster:            " +  configuration.getString(ConfigProperty.CASSANDRA_CLUSTER_NAME.name()));
            System.out.println("Cassandra Keyspace:           " + configuration.getString(ConfigProperty.CASSANDRA_KEYSPACE.name()));
            System.out.println("Cassandra Replication Factor: " + configuration.getInteger(ConfigProperty.CASSANDRA_REPLICATION_FACTOR.name(), 1));
            System.out.println("THREADS:                      " + configuration.getInteger("THREADS", 2));
            System.out.println("BATCH_SIZE:                   " + configuration.getInteger("BATCH_SIZE", 2));
            System.out.println("DRY_RUN:                      " + isDryRun);
            System.out.println("Tenant:                       " + tenantId );
            System.out.println("StartKey:                     " + startCK);
            System.out.println("INPUT_DATA_FILE:           " + inputDataFile     );
            System.out.println("Types:                        " + ((types == null || types.isEmpty())? "<>" :
                    types.stream().collect(Collectors.joining(", "))));
                    //));
            System.out.println("Suffix:                       " + suffix);


            ReltioPlatform.Builder platformBuilder = new ReltioPlatform.Builder()
                    .setConfiguration(configuration)
                    .setTenantsFilter(Collections.singletonList(tenantId.getId()));
            ReltioPlatform platform = null;
            try{
                platform = platformBuilder.build();
                System.out.println("Export Platform started...");
                ScanAllEntitiesByTypeByRangeSplit scanAllEntitiesByType  =  new ScanAllEntitiesByTypeByRangeSplit(platform, tenantId, threads, batch);
                //scanAllEntitiesByType.scanAllEntityByRanges(types, suffix);
                scanAllEntitiesByType.purgeGivenExternalXW(inputDataFile);
            }
            finally {
                System.out.println("EXPORT FINISHED!!!");
                if (platform != null){
                    platform.shutdown();
                }
            }
        }
    }
}
