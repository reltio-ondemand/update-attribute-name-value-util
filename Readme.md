**Description**
Update Cassandra utility is developed to scan Cassandra Tables for  ENTITY, RELATION , CROSSWALK tables
This utiltiy then based on the filter criteria can modify the Cassandra data. 

Please be sure that you check and test the logic and the data filter before running this script
As This will modify the tenant Data. 

**How To build and deploy the script** 
1. checkout the repo 
2. execute command `mvn clean install`
3. Copy the fat JAR(This jar will include all the dependencies and will be much larger in the size) from the target
   folder to the desired server and location that have connectivity to C* servers  
4. Copy the source file from the resouce folder and update as needed.   

To run the jar you can create a script as added in the  resource folder as sample 



**Properties** 
Most of he Properties values can be found in the {{api_server}}/admin/systemConfig/ REST API Response 
`CASSANDRA_HOSTS=host1, host2  #hostname of the cassandra servers this will be used to get details about the envi and
 the tenant.  
CASSANDRA_CLUSTER_NAME=ReltioCluster  
CASSANDRA_KEYSPACE=ReltioKeyspace
CASSANDRA_REPLICATION_FACTOR=1
reltio.environment.name=tests

_*This should be the tenant name that user needs to remediate please make sure to this is the correct value_
tenant=LocalTest  

  
_*Depending on the operation this number need to be updated. Please work with smaller number first_
THREADS=1000000  
BATCH_SIZE=1000  
 
CASSANDRA_CONNECTION_TYPE=SSL  

_*For some operation below details are Optional so please check the code or contact the Author_ 
DRY_RUN=true  # may not be applicable for some operation.

SUFFIX=001   
START_FROM_KEY=0000aCc001  #not Needed and can be left as is  
INPUT_DATA_FILE=/Users/prashant.shimpi/Documents/workspace/artem/input_deleteEXW.txt  
  
`
**MODE**
As of not Modes are not configured so need to update the code to execute a specific mode type 
1. scanAllEntityByRanges : Scan all Entities and check and filter for the crosswalk (hard coded )
   _*This will scan all entity type if the ENTITY_TYPE property is not set or specify the ENTITY_TYPE if need 
   to filter by a specific type e.g.  ENTITY_TYPE=HCP, HCO 
   _*SUFFIX needs to be taken from the Cassandra records it is usaually a 3 letter alpha-numeric value present in the
    database
    _please try to use below  values 
    BATCH_SIZE=50 
    THREADS=200  
    START_FROM_KEY=0000aCc001 _*it is not used 
    INPUT_DATA_FILE  is not needed 
2. scanAllRelationsByRanges :
         _*This will scan all Relations and ENTITY_TYPE is not used and optional as it is not used for filtering 
       _*SUFFIX needs to be taken from the Cassandra records it is usually a 3 letter alpha-numeric value present in the
        database
        _please try to use below  values 
        BATCH_SIZE=50 
        THREADS=200  
        INPUT_DATA_FILE  is not needed 
3. scanAllExternalCrosswalksByRanges  
       _*This will scan all external crosswalk table and apply filter (hard coded ) 
            SUFFIX : not applicable in this case can leave as is  
            _please try to use below  values 
            BATCH_SIZE=50 
            THREADS=200  
            INPUT_DATA_FILE  is not needed 
4. scanAllExternalXW : 
         _*Purges crosswalk from the external crosswalk for the given input file.
         INPUT_DATA_FILE  is needed please provide full path of the file
         SUFFIX : not applicable in this case can leave as is
         below value worked better for some large inputs 
         THREADS=1000000
         BATCH_SIZE=100

**Author** prashant.shimpi@reltio.com 
    
