import com.reltio.db.layer.base.objects.IEntityURI;
import com.reltio.db.layer.base.objects.impl.read.ObjectRange;
import com.reltio.db.layer.base.read.DBReadOptions;
import com.reltio.db.layer.base.read.IObjectRange;
import com.reltio.db.layer.cassandra.executor.CassandraExecutorEntitiesDAO;
import com.reltio.iterators.Batch;
import com.reltio.iterators.IteratorWithLastKey;
import com.reltio.metadata.uri.TenantId;
import com.reltio.rest.data.marshalling.IMarshaller;
import com.reltio.sdk.platform.services.ReltioPlatform;
import com.reltio.storage.cassandra.concurrent.CassandraConcurrentExecutor;
import com.reltio.storage.cassandra.concurrent.ConcurrentExecutorException;
import com.reltio.storage.cassandra.exec.CassandraExecutor;
import com.reltio.storage.cassandra.reader.IRowReader;
import com.reltio.storage.cassandra.tenancy.ITenantsStorage;
import com.reltio.storage.cassandra.tenancy.TenantDataAccess;
import com.reltio.storage.cassandra.to.CassandraDataKeyFactory;
import com.reltio.storage.cassandra.to.CassandraHColumn;
import com.reltio.storage.cassandra.to.CassandraKeyFactory;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ScanAllEntitiesByType {

    private final int thread;
    private final int batchSize;
    private ReltioPlatform platform;
    private TenantId tenantId;
    FileAppender dataAppender;
    private final ITenantsStorage tenantsStorage;
    private IMarshaller marshaller;
    private TenantDataAccess tenantDataAccess;
    private CassandraDataKeyFactory keyFactory;
    private CassandraExecutor cassandraExecutor;
    private CassandraExecutorEntitiesDAO entitiesDAO;
    protected final Consumer<String> keyHandler = this::updateLastDataKey;
    private String lastProcessedKey;

    private void updateLastDataKey(String s) {
        this.lastProcessedKey = s;
    }

    public ScanAllEntitiesByType(ReltioPlatform platform, TenantId tenantId, int thread, int batchSize) {
        this.platform = platform;
        this.marshaller = platform.getMarshaller();
        this.tenantDataAccess = platform.getTenantDataAccess();
        tenantsStorage = this.tenantDataAccess.getTenantsStorage();
        this.tenantId = tenantId;
        long t1 = System.currentTimeMillis();
        this.dataAppender = new FileAppender(new File("./", t1 + "_data.log").getPath(), true);
        waitForTenantLoaded();
        entitiesDAO = (CassandraExecutorEntitiesDAO) tenantDataAccess.getEntitiesDAO(tenantId);
        cassandraExecutor = entitiesDAO.getCassandraConcurrentExecutor().getCassandraExecutor();
        keyFactory = new CassandraDataKeyFactory(tenantDataAccess.getTenantsStorage());
        this.thread = thread;
        this.batchSize = batchSize;
    }

    private void waitForTenantLoaded() {
        int waitTime = 400;
        dataAppender.accept("Waiting for loading tenant " + tenantId + " max wait time, seconds : " + waitTime);
        long timeout = 0;
        while (!tenantDataAccess.isFullyLoaded(tenantId) && TimeUnit.SECONDS.toMillis(waitTime) > timeout) {
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
            }
            timeout += 400;
        }
        if (timeout > TimeUnit.SECONDS.toMillis(120)) {
            dataAppender.accept("Timeout exceeded while waiting for tenant, skipping " + tenantId);
        }
    }

    public void scanAllEntity(Set<String> types,String startKey, String suffix) throws InterruptedException {
        long t1 = System.currentTimeMillis();
        CassandraExecutor cassandraExecutor = entitiesDAO.getCassandraExecutor();
        keyFactory = new CassandraDataKeyFactory(tenantsStorage);
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(thread);

        IObjectRange range = ObjectRange.getSingleRange(null, null);
        IteratorWithLastKey<IEntityURI> iterator = entitiesDAO.getAllObjectUrisFromRange(range, types, null, batchSize, DBReadOptions.DEFAULT);
        AtomicLong batchCount = new AtomicLong(0);
        AtomicLong recordCount = new AtomicLong(0);

        List<BatchWorker> tasks = new ArrayList<>();
        while (iterator.hasNextBatch(keyHandler)) {
            Batch<IEntityURI> batch = iterator.nextBatch();
            Collection<IEntityURI> buffer = batch.getPack();
            updateLastDataKey(batch.getLastKey());

            BatchWorker batchWorker = new BatchWorker(buffer, cassandraExecutor, keyFactory, recordCount, batchCount,
                    dataAppender, false, suffix);
            tasks.add(batchWorker);
            try {
                if (tasks.size() == batchSize) {
                    List<Future<Boolean>> futures = executorService.invokeAll(tasks);
                    boolean isFound = false;
                    for (Future<Boolean> future : futures) {
                        if (future.get()) isFound = true;
                        future.isDone();
                    }
                    tasks.clear();
                    dataAppender.accept("batchLastRecord=" + lastProcessedKey);
                    if (isFound) break;
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        if (!tasks.isEmpty()) {
            List<Future<Boolean>> futures = executorService.invokeAll(tasks);
            futures.forEach(f -> f.isDone());
            tasks.clear();
        }
        long t2 = System.currentTimeMillis();
        long time = (t2 - t1);
        dataAppender.accept("time in Min =" + time);
        dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount.incrementAndGet(), recordCount.get()));
        dataAppender.finalHook();
        executorService.shutdown();
    }

    private static class BatchWorker implements Callable<Boolean> {
        Collection<IEntityURI> buffer;
        CassandraKeyFactory keyFactory;
        AtomicLong recordCount;
        AtomicLong batchCount;
        CassandraConcurrentExecutor cassandraExecutor;
        private FileAppender summary;
        FileAppender dataAppender;
        Boolean isFound;
        String suffix;


        public BatchWorker(Collection<IEntityURI> buffer, CassandraExecutor cassandraExecutor,
                           CassandraKeyFactory keyFactory, AtomicLong recordCount, AtomicLong batchCount,
                           FileAppender dataAppender, Boolean isFound, String suffix) {
            this.buffer = buffer;
            this.cassandraExecutor = new CassandraConcurrentExecutor(cassandraExecutor);
            this.keyFactory = keyFactory;
            this.recordCount = recordCount;
            this.batchCount = batchCount;
            this.dataAppender = dataAppender;
            this.isFound = isFound;
            this.suffix = suffix;
        }

        @Override
        public Boolean call() throws InterruptedException, ExecutionException, ConcurrentExecutorException {
            cassandraExecutor.read("ENTITIES",
                    buffer.parallelStream().map(e -> keyFactory.
                            buildCassandraKey(e.getID() + suffix)).collect(Collectors.toSet()), null,
                    (IRowReader) (k, v) -> {
                        dataAppender.accept(k.getCassandraKey());
                        recordCount.incrementAndGet();
                        for (Map.Entry<String, CassandraHColumn> entry : v.entrySet()) {
                            if (entry.getKey().startsWith("A.ID.")) {
                                String value = entry.getValue().getValue();
                                // if(value.equals(""))
                                String val = entry.getValue().getValue();
                                String key = entry.getKey();
                                String rowKey = k.getId();
                                if (val.equals("836520699")) {
                                    dataAppender.accept(String.format("row_key=%s, key=%s val=%s", rowKey, key, val));
                                    System.out.println(String.format("row_key=%s, key=%s val=%s", rowKey, key, val));
                                    isFound = true;
                                    break;
                                }

                            }
                        }
                    });
            dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount.incrementAndGet(), recordCount.get()));
            return isFound;
        }
    }
}
