import com.reltio.common.Pair;
import com.reltio.db.layer.base.objects.IEntityURI;
import com.reltio.db.layer.base.objects.IRelationStub;
import com.reltio.db.layer.base.objects.impl.crosswalk.ExternalCrosswalkRecord;
import com.reltio.db.layer.base.objects.impl.object.TenantID;
import com.reltio.db.layer.base.objects.impl.read.ObjectRange;
import com.reltio.db.layer.base.read.DBReadOptions;
import com.reltio.db.layer.base.read.IObjectRange;
import com.reltio.db.layer.base.read.IObjectRanges;
import com.reltio.db.layer.cassandra.executor.CassandraExecutorEntitiesDAO;
import com.reltio.db.layer.cassandra.executor.CassandraExecutorRelationsDAO;
import com.reltio.db.layer.cassandra.executor.ranges.ObjectRangesUtils;
import com.reltio.iterators.Batch;
import com.reltio.iterators.IteratorWithLastKey;
import com.reltio.metadata.uri.TenantId;
import com.reltio.rest.data.marshalling.IMarshaller;
import com.reltio.sdk.platform.services.ReltioPlatform;
import com.reltio.storage.cassandra.exec.CassandraExecutor;
import com.reltio.storage.cassandra.reader.IRowReader;
import com.reltio.storage.cassandra.tenancy.ITenantsStorage;
import com.reltio.storage.cassandra.tenancy.TenantDataAccess;
import com.reltio.storage.cassandra.to.CassandraDataKeyFactory;
import com.reltio.storage.cassandra.to.CassandraHColumn;
import com.reltio.storage.cassandra.to.CassandraKey;
import com.reltio.storage.cassandra.to.CassandraKeyFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.hadoop.hbase.shaded.org.mortbay.util.SingletonList;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ScanAllEntitiesByTypeByRangeSplit {

    private final int thread;
    private final int batchSize;
    private ReltioPlatform platform;
    private TenantId tenantId;
    FileAppender dataAppender;
    FileAppender resultAppender;
    private final ITenantsStorage tenantsStorage;
    private IMarshaller marshaller;
    private TenantDataAccess tenantDataAccess;
    private CassandraDataKeyFactory keyFactory;
    private CassandraExecutor cassandraExecutor;
    private CassandraExecutorEntitiesDAO entitiesDAO;
    private CassandraExecutorRelationsDAO relationsDAO;

    private String lastProcessedKey;

    private void updateLastDataKey(String s) {
        this.lastProcessedKey = s;
    }

    public ScanAllEntitiesByTypeByRangeSplit(ReltioPlatform platform, TenantId tenantId, int thread, int batchSize) {
        this.platform = platform;
        this.marshaller = platform.getMarshaller();
        this.tenantDataAccess = platform.getTenantDataAccess();
        tenantsStorage = this.tenantDataAccess.getTenantsStorage();
        this.tenantId = tenantId;
        long t1 = System.currentTimeMillis();
        this.dataAppender = new FileAppender(new File("./", t1 + "_data.log").getPath(), true);
        this.resultAppender = new FileAppender(new File("./", t1 + "_result.log").getPath(), true);
        waitForTenantLoaded();
        entitiesDAO = (CassandraExecutorEntitiesDAO) tenantDataAccess.getEntitiesDAO(tenantId);
        relationsDAO = (CassandraExecutorRelationsDAO) tenantDataAccess.getRelationsDAO(tenantId);
        cassandraExecutor = entitiesDAO.getCassandraConcurrentExecutor().getCassandraExecutor();
        keyFactory = new CassandraDataKeyFactory(tenantDataAccess.getTenantsStorage());
        this.thread = thread;
        this.batchSize = batchSize;
    }

    private void waitForTenantLoaded() {
        int waitTime = 400;
        dataAppender.accept("Waiting for loading tenant " + tenantId + " max wait time, seconds : " + waitTime);
        long timeout = 0;
        while (!tenantDataAccess.isFullyLoaded(tenantId) && TimeUnit.SECONDS.toMillis(waitTime) > timeout) {
            try {
                Thread.sleep(400);
            } catch (InterruptedException e) {
            }
            timeout += 400;
        }
        if (timeout > TimeUnit.SECONDS.toMillis(120)) {
            dataAppender.accept("Timeout exceeded while waiting for tenant, skipping " + tenantId);
        }
    }

    public void scanAllEntityByRanges(Set<String> types, String suffix) throws InterruptedException {
        long t1 = System.currentTimeMillis();
        keyFactory = new CassandraDataKeyFactory(tenantsStorage);
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(thread);
        AtomicLong batchCount = new AtomicLong(0);
        AtomicLong recordCount = new AtomicLong(0);
        IObjectRanges ranges = entitiesDAO.splitDatasetToFixedRanges(thread);
        List<BatchWorker2> tasks = new ArrayList<>();
        for (int i = 0; i < ranges.getRangesCount(); i++) {
            BatchWorker2 batchWorker2 = new BatchWorker2(ranges.getRange(i),
                    entitiesDAO,
                    keyFactory,
                    recordCount,
                    batchCount,
                    dataAppender,
                    resultAppender,
                    false, suffix, types, batchSize);
            tasks.add(batchWorker2);

        }
        List<Future<Boolean>> futures = executorService.invokeAll(tasks);
        futures.forEach(Future::isDone);

        long t2 = System.currentTimeMillis();
        long time = (t2 - t1);
        dataAppender.accept("time in Min =" + time);
        dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount.incrementAndGet(), recordCount.get()));
        dataAppender.finalHook();
        resultAppender.finalHook();
        executorService.shutdown();
    }


    public void scanAllRelationsByRanges(Set<String> types, String suffix) throws InterruptedException {
        long t1 = System.currentTimeMillis();
        keyFactory = new CassandraDataKeyFactory(tenantsStorage);
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(thread);
        AtomicLong batchCount = new AtomicLong(0);
        AtomicLong recordCount = new AtomicLong(0);
        IObjectRanges ranges = relationsDAO.splitDatasetToFixedRanges(thread);
        List<BatchWorkerRel> tasks = new ArrayList<>();
        for (int i = 0; i < ranges.getRangesCount(); i++) {
            BatchWorkerRel batchWorkerRel = new BatchWorkerRel(ranges.getRange(i),
                    relationsDAO,
                    keyFactory,
                    recordCount,
                    batchCount,
                    dataAppender,
                    resultAppender,
                    false, suffix, types, batchSize);
            tasks.add(batchWorkerRel);

        }
        List<Future<Boolean>> futures = executorService.invokeAll(tasks);
        futures.forEach(Future::isDone);

        long t2 = System.currentTimeMillis();
        long time = (t2 - t1);
        dataAppender.accept("time in Min =" + time);
        dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount.incrementAndGet(), recordCount.get()));
        dataAppender.finalHook();
        resultAppender.finalHook();
        executorService.shutdown();
    }

    public void scanAllExternalCrosswalksByRanges(Set<String> types, String suffix) throws InterruptedException {
        long t1 = System.currentTimeMillis();
        keyFactory = new CassandraDataKeyFactory(tenantsStorage);
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(batchSize);
        AtomicLong batchCount = new AtomicLong(0);
        AtomicLong recordCount = new AtomicLong(0);
        String crosswalkCF =
                tenantDataAccess.getTenantsStorage().getTenantConfiguration(tenantId).getDataStorageConfig().getExternalCrosswalkCF();
        TenantID tenantID = new TenantID(tenantId.getId(), tenantsStorage.getTenantIdentifier(tenantId));
        IObjectRanges ranges = ObjectRangesUtils.getRanges(tenantID, this.keyFactory, this.cassandraExecutor, -1,
                crosswalkCF, thread);
        List<BatchWorkerCrosswalks> tasks = new ArrayList<>();

        List<Future<Boolean>> futures = new ArrayList<>();
        int batchRecCnt = 0;
        for (int i = 0; i < ranges.getRangesCount(); i++) {
            BatchWorkerCrosswalks batchWorkerCrosswalks = new BatchWorkerCrosswalks(ranges.getRange(i),
                    cassandraExecutor,
                    recordCount,
                    batchCount,
                    dataAppender,
                    resultAppender,crosswalkCF);
                tasks.add(batchWorkerCrosswalks);
            if (tasks.size() % batchSize == 0) {
                futures.addAll(executorService.invokeAll(tasks));
                tasks.clear();
                dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount.incrementAndGet(), recordCount.get()));
            }
            batchRecCnt++;
        }

        if (!tasks.isEmpty()) {
            System.out.println("invoke after  batch");
            futures.addAll(executorService.invokeAll(tasks));
        }

        futures.forEach(Future::isDone);
        long t2 = System.currentTimeMillis();
        long time = (t2 - t1);
        dataAppender.accept("time in Min =" + time);
        dataAppender.finalHook();
        resultAppender.finalHook();
        executorService.shutdown();
    }


    public void purgeGivenExternalXW(String inputDataFile) throws InterruptedException {
        long t1 = System.currentTimeMillis();
        keyFactory = new CassandraDataKeyFactory(tenantsStorage);
        AtomicLong recordCount = new AtomicLong(0);

        try {
              if(inputDataFile != null|| !inputDataFile.trim().equals("")) {
                  LineIterator records = FileUtils.lineIterator(new File(inputDataFile), "UTF-8");
                  while(records.hasNext()) {

                      Set<String> temp = new HashSet();
                      String xw = records.next();
                      System.out.println(xw);
                      temp.add(xw);
                      dataAppender.accept(String.format("count=%s, xw=%s",  recordCount.incrementAndGet(), xw));
                      entitiesDAO.deleteExternalCrosswalkRecords(temp);
                  }
              }else{
                  System.out.println("Input file is not present");
              }
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataAppender.finalHook();
        resultAppender.finalHook();
    }

    private static class BatchWorker2 implements Callable<Boolean> {
        IObjectRange range;
        CassandraKeyFactory keyFactory;
        AtomicLong recordCount;
        AtomicLong batchCount;
        CassandraExecutorEntitiesDAO entitiesDAO;
        private FileAppender summary;
        FileAppender dataAppender;
        FileAppender resultAppender;
        Boolean isFound;
        String suffix;
        Set<String> types;
        int batchSize;
        String lastProcessedKey;
        protected final Consumer<String> keyHandler = this::updateLastDataKey;

        private void updateLastDataKey(String s) {
            this.lastProcessedKey = s;
        }

        public BatchWorker2(IObjectRange range, CassandraExecutorEntitiesDAO entitiesDAO,
                            CassandraKeyFactory keyFactory, AtomicLong recordCount, AtomicLong batchCount,
                            FileAppender dataAppender, FileAppender resultAppender, Boolean isFound, String suffix,
                            Set<String> types,
                            int batchSize) {
            this.range = range;
            this.entitiesDAO = entitiesDAO;
            this.keyFactory = keyFactory;
            this.recordCount = recordCount;
            this.batchCount = batchCount;
            this.dataAppender = dataAppender;
            this.resultAppender = resultAppender;
            this.isFound = isFound;
            this.suffix = suffix;
            this.types = types;
            this.batchSize = batchSize;
        }

        @Override
        public Boolean call() {
            IteratorWithLastKey<IEntityURI> iterator = entitiesDAO.getAllObjectUrisFromRange(range, types, null,
                    batchSize, DBReadOptions.DEFAULT);

            while (iterator.hasNextBatch(keyHandler)) {
                Batch<IEntityURI> batch = iterator.nextBatch();
                Collection<IEntityURI> buffer = batch.getPack();
                CassandraExecutor cassandraExecutor = entitiesDAO.getCassandraExecutor();
                cassandraExecutor.read("ENTITIES",
                        buffer.parallelStream().map(e -> keyFactory.
                                buildCassandraKey(e.getID() + suffix)).collect(Collectors.toSet()), null,
                        (IRowReader) (k, v) -> {
                            dataAppender.accept(k.getCassandraKey());
                            recordCount.incrementAndGet();
                            for (Map.Entry<String, CassandraHColumn> entry : v.entrySet()) {
                                if (entry.getKey().startsWith("A.ID.")) {
                                    String value = entry.getValue().getValue();
                                    // if(value.equals(""))
                                    String val = entry.getValue().getValue();
                                    String key = entry.getKey();
                                    String rowKey = k.getId();
                                    if (val.equals("836520699")) {
                                        resultAppender.accept(String.format("row_key=%s, key=%s val=%s", rowKey, key, val));
                                        System.out.println(String.format("row_key=%s, key=%s val=%s", rowKey, key, val));
                                    }

                                }
                            }
                        });
            }
            dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount.incrementAndGet(), recordCount.get()));
            return false;
        }
    }

    private static class BatchWorkerRel implements Callable<Boolean> {
        IObjectRange range;
        CassandraKeyFactory keyFactory;
        AtomicLong recordCount;
        AtomicLong batchCount;
        CassandraExecutorRelationsDAO relationsDAO;
        private FileAppender summary;
        FileAppender dataAppender;
        FileAppender resultAppender;
        Boolean isFound;
        String suffix;
        Set<String> types;
        int batchSize;
        String lastProcessedKey;
        protected final Consumer<String> keyHandler = this::updateLastDataKey;

        private void updateLastDataKey(String s) {
            this.lastProcessedKey = s;
        }

        public BatchWorkerRel(IObjectRange range, CassandraExecutorRelationsDAO relationsDAO,
                              CassandraKeyFactory keyFactory, AtomicLong recordCount, AtomicLong batchCount,
                              FileAppender dataAppender, FileAppender resultAppender, Boolean isFound, String suffix,
                              Set<String> types,
                              int batchSize) {
            this.range = range;
            this.relationsDAO = relationsDAO;
            this.keyFactory = keyFactory;
            this.recordCount = recordCount;
            this.batchCount = batchCount;
            this.dataAppender = dataAppender;
            this.resultAppender = resultAppender;
            this.isFound = isFound;
            this.suffix = suffix;
            this.types = types;
            this.batchSize = batchSize;
        }

        @Override
        public Boolean call() {

            IteratorWithLastKey<Pair<IEntityURI, Collection<IRelationStub>>> iterator = relationsDAO.scanRelations(range, null, 100);

            while (iterator.hasNextBatch(keyHandler)) {
                Batch<Pair<IEntityURI, Collection<IRelationStub>>> batch = iterator.nextBatch();
                Collection<Pair<IEntityURI, Collection<IRelationStub>>> buffer = batch.getPack();
                CassandraExecutor cassandraExecutor = relationsDAO.getCassandraExecutor();
                Set<String> relations = new HashSet<>();

                buffer.stream().forEach(pair -> pair.b.forEach(rel -> relations.add(rel.getID())));

                cassandraExecutor.read("RELATIONS",
                        relations.parallelStream().map(e -> keyFactory.
                                buildCassandraKey(e + suffix)).collect(Collectors.toSet()), null,
                        (IRowReader) (k, v) -> {
                            dataAppender.accept(k.getCassandraKey());
                            recordCount.incrementAndGet();
                            for (Map.Entry<String, CassandraHColumn> entry : v.entrySet()) {
                                if (entry.getKey().startsWith("XRE") || entry.getKey().startsWith("XRS") || entry.getKey().startsWith("XRZ")) {
                                    String value = entry.getValue().getValue();
                                    //if(value.equals(""))
                                    String val = entry.getValue().getValue();
                                    String key = entry.getKey();
                                    String rowKey = k.getId();
                                    if (val != null && (val.contains("FIJI") || val.contains("MONACO")))
                                        resultAppender.accept(String.format("row_key=%s, key=%s val=%s", rowKey, key, val));
                                    //System.out.println(String.format("row_key=%s, key=%s val=%s", rowKey, key,
                                    //    val));
                                }
                            }
                        });
            }
            dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount.incrementAndGet(), recordCount.get()));
            return false;
        }
    }

    private static class BatchWorkerCrosswalks implements Callable<Boolean> {
        IObjectRange range;
        CassandraKeyFactory keyFactory;
        AtomicLong recordCount;
        AtomicLong batchCount;
        CassandraExecutor cassandraExecutor;
        FileAppender dataAppender;
        FileAppender resultAppender;
        String cf;

        public BatchWorkerCrosswalks(IObjectRange range,
                                     CassandraExecutor cassandraExecutor,
                                     AtomicLong recordCount,
                                     AtomicLong batchCount,
                                     FileAppender dataAppender,
                                     FileAppender resultAppender,
                                     String cf) {
            this.range = range;
            this.cassandraExecutor = cassandraExecutor;
            this.recordCount = recordCount;
            this.batchCount = batchCount;
            this.dataAppender = dataAppender;
            this.resultAppender = resultAppender;
            this.cf = cf;
        }

        @Override
        public Boolean call() {
            ObjectRange objectRange = (ObjectRange) range;
            List<Pair<CassandraKey, Map<String, CassandraHColumn>>> crosswalks = cassandraExecutor.getRowsBetweenTokens(
                    cf, null, objectRange.getStartToken(),
                    objectRange.getEndToken());
            for (Pair<CassandraKey, Map<String, CassandraHColumn>> pair : crosswalks) {
                recordCount.incrementAndGet();
                String val = pair.a.getCassandraKey();
                if (val != null && (val.contains("FIJI") || val.contains("MONACO"))) {
                    String msg = String.format("val=%s, key=%s", val, pair.b.keySet().stream().collect(Collectors.joining(",")));
                    resultAppender.accept(msg);
                }
            }
            dataAppender.accept(String.format("batchCount=%s, count=%s", batchCount, recordCount.get()));
            return false;
        }
    }

}
